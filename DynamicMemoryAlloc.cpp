#include <iostream>
#include <string>
#include <Student.h>
using namespace std;

void computePTuition(Student(&student), int units) {
    int* fee = new int;
    auto* pTuition = new double;
    *fee = 1030;
    *pTuition = *fee * units;
    student.setPartialTuition(*pTuition);
    delete fee;
    delete pTuition;
}
int main() {
    string name, course;
    int year, units;
    cout << "Name of the student: ";
    cin >> name;
    cout << "Course: ";
    cin >> course;
    cout << "Year: ";
    cin >> year;

    Student student(name, course, year);
    student.printStudent();

    cout << "\nUnits: ";
    cin >> units;
    computePTuition(student, units);
    cout << "Tuition for Major Subjects: " << student.getPartialTuition();
    return 0;
}