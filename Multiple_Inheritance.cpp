#include <iostream>
using namespace std;

class Person
{
    string name;
    string gender;
    int age;

public:
    Person()
    {
        name = "Juan Cruz";
        gender = "male";
        age = 21;
    }

    string getName()
    {
        return name;
    }

    string getGender()
    {
        return gender;
    }

    int getAge()
    {
        return age;
    }

    void printPerson()
    {
        cout << name << ", " << gender << ", " << age << endl;
    }
};

class Employee
{
    string job;
    double salary;

public:
    Employee()
    {
        job = "Computer Programmer";
        salary = 12345.60;
    }

    string getJob()
    {
        return job;
    }

    double getSalary()
    {
        return salary;
    }

    void printEmployee()
    {
        cout << job << ", " << salary << endl;
    }
};

class Citizen : public Person, public Employee
{
    string address = "Baguio city";

public:
    Citizen()
    {
        address = "Baguio city";
    }

    string getAddress()
    {
        return address;
    }
};

int main()
{
    Citizen citizen;
    citizen.printPerson();
    citizen.printEmployee();
    cout << citizen.getAddress();
    return 0;
}
