// Checks whether the input number is a prime or not

#include <iostream>

using namespace std;

int main()
{
    int num;
    cout << "Enter a number: ";
    cin >> num;
    int b = 2;

    bool prime = true;
    while(b != num)
    {
        if (num%b == 0)
        {
            prime = false;
            break;
        }
    b++;
    }

    if (prime)
        cout << num << " is a prime number";
    else
        cout << num << " is not a prime number";

    return 0;
}
