// Sample fibonacci series program applying the process of recursion

#include<iostream> // include statement

using namespace std;

// prints the series
void printFibonacci(int n)
{
    static int n1 = 0, n2 = 1, n3;

    if (n > 0)
    {
         n3 = n1 + n2;
         n1 = n2;
         n2 = n3;
         cout << n3 << " ";
         printFibonacci(n-1);
    }
}

int main()
{
    int n;
    cout << "Enter the number of elements: ";
    cin >> n;
    cout << "Fibonacci Series: ";
    cout << "0 " << "1 ";

    printFibonacci(n-2);  // subtract the value by two since two integers are already printed
    return 0;
}
