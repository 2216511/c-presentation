// Returns the ASCII code of the corresponding input character or number

#include <iostream> // include declaration

using namespace std;

int main()
{
  char ascii;
  int numeric;

  cout << "Enter character: ";
  cin >> ascii;
  cout << "Its ascii value is: " << (int) ascii << endl;

  cout << "Enter a number to convert to ascii: ";
  cin >> numeric;
  cout << "The ascii value of " << numeric << " is " << (char) numeric;

  return 0;
}
