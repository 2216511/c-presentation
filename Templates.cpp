#include <iostream>
using namespace std;

template <typename T> T myMax(T x, T y)
{
    //This method returns what value is larger than the other
    return (x > y) ? x : y;
}

int main()
{
    int x = 90, y = 85;
    float a = 60.123, b = 80.765;
    double r = 100.123, s = 986.928;
    cout << myMax<int>(x, y) << endl; // call myMax for int
    cout << myMax<float>(a, b) << endl; // call myMax for float
    cout << myMax<double>(r, s) << endl; // call myMax for double
    cout << myMax<string>("long", "short") << endl;
    cout << myMax<char>('a', 'z') << endl;
    return 0;
}