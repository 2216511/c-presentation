#include <iostream>

using namespace std;

//swap values for given indices
void swap(int* x, int* y)
{
    int temp = *x;  //dereference
    *x = *y;
    *y = temp;
}

void sortArray(int(&aref)[10], int n) //aref array as an alias of the 'a' array
{
    int i, j, min;
    for (i = 0; i < n - 1; i++)
    {
        min = i;
        for (j = i + 1; j < n; j++)
        {
            if (aref[j] < aref[min])
            {
                min = j;
            }
        }
        swap(&aref[i], &aref[min]);
    }
}

int main()
{
    int a[] = { 22, 91, 35, 78, 10, 8, 75, 99, 1, 67 };
    int n = sizeof(a) / sizeof(a[0]);
    int i;

    //display the unsorted array on the output screen
    cout<<"Given array is:"<<endl;
    for (i = 0; i < n; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;

    //sort the array in ascending order
    sortArray(a, n);

    //display the sorted array on the output screen
    printf("\nSorted array is: \n");
    for (i = 0; i < n; i++)
    {
        cout<< a[i] <<" ";
    }
    return 0;
}

/*int main()
{
    int var = 1;
    int& ref = var;
    int* ptr = &var;

    cout << ref << endl;
    cout << var << endl;
    ref = 8;
    cout << var << endl;
    cout << ptr << endl;

    return 0;
}*/
