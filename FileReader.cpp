///*

// Version 1 with using namespace std declaration//

/*
    This class named FileReader...
*/

// include statements
#include <iostream>
#include <fstream>
#include <string>

using namespace std;


/*
    This is the main method declaration that makes
    the FileReader class executable. It accepts an
    input from the user and that input will be used
    as the filename of the file that will be read.
*/
int main()
{
    string filename;
    string answer = "A";
    bool loop = true;

    do {
        cout << "Enter the filename you want to read: ";
        cin >> filename;
        cout << "-------------------------------------------------------" << '\n';

        /*
        ifstream file;
        file.open(filename);
        string line;
        */
    
        if (file.is_open()) {
            while (file) {
                getline (file, line);
                cout << line << '\n';
            }
        }
        else {
            cout << "Error! File not found!" << endl; // print an error message
        }

        do {
            cout << "-------------------------------------------------------" << '\n';
            cout << "Do you want to read another file? [Y/N] : ";
            cin >> answer; // instantiate the answer variable using the user input
            if (answer == "N" || answer == "n") {
                cout << "-------------------------------------------------------" << '\n';
                cout << "Program Terminated..." << endl;
                loop = false;
            } else if (answer != "Y" && answer != "N" && answer != "y" && answer != "n") {
                cout << "-------------------------------------------------------" << '\n';
                cout << "Error! Please input 'Y'|'y' for yes and 'N'|'n' for no." << endl;
            }
        }
        while (answer != "Y" && answer != "N" && answer != "y" && answer != "n");
    }
    while (loop);
    return 0;      
}
//*/

/*

// Version 2 without using namespace std declaration//
#include <iostream>
#include <fstream>
#include <string>

int main ()
{
    std::string filename;
    std::string answer = "A";
    bool loop = true;

    do {
        std::cout << "Enter the filename you want to read: ";
        std::cin >> filename;
        std::cout << "-------------------------------------------------------" << '\n';

        std::ifstream file;
        file.open(filename);
        std::string line;
	std::cout << "this is a new freaking line!; 
    
        if (file.is_open()) {
            while (file) {
                std::getline (file, line);
                std::cout << line << '\n';
            }
        }
        else {
            std::cout << "Error! File not found!" << std::endl;
        }

        do {
            std::cout << "-------------------------------------------------------" << '\n';
            std::cout << "Do you want to read another file? [Y/N] : ";
            std::cin >> answer;
            if (answer == "N" || answer == "n") {
                std::cout << "-------------------------------------------------------" << '\n';
                std::cout << "Program terminated..." << std::endl;
                loop = false;
            } else if (answer != "Y" && answer != "N" && answer != "y" && answer != "n") {
                std::cout << "-------------------------------------------------------" << '\n';
                std::cout << "Error! Please input 'Y'|'y' for yes and 'N'|'n' for no." << std::endl;
            }
        }
        while (answer != "Y" && answer != "N" && answer != "y" && answer != "n");

    }
    while (loop);
    return 0;
}
*/
