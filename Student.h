//
// Created by user on 19/09/2022.
//

#ifndef C_PRESENTATION_STUDENT_H
#define C_PRESENTATION_STUDENT_H
#include <string>
using namespace std;

class Student
{
private:
    string name;
    string course;
    int year;
    double partialTuition;

public:
    Student(string n, string c, int y); //construction declaration
    void setName(string n);
    void setCourse(string c);
    void setYear(int y);
    void setPartialTuition(double pt);
    string getName();
    string getCourse();
    int getYear();
    double getPartialTuition();
    void printStudent();
};


#endif //C_PRESENTATION_STUDENT_H
