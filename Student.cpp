#include <iostream>
#include <string>
#include <Student.h>
using namespace std;

// Constructor definition outside the class
Student::Student(string n, string c, int y) {
    name = std::move(n);
    course = std::move(c);
    year = y;
}

void Student::setName(string n) {
    name = std::move(n);
}

void Student::setCourse(string c) {
    course = std::move(c);
}

void Student::setYear(int y) {
    year = y;
}

void Student::setPartialTuition(double pt) {
    partialTuition = pt;
}

string Student::getName() {
    return name;
}

string Student::getCourse() {
    return course;
}

int Student::getYear() {
    return year;
}

double Student::getPartialTuition() {
    return partialTuition;
}
void Student::printStudent() {
    cout << name << ", " << course << ", year level: " << year << endl;
}
