#include <iostream>
#include <math.h>
using namespace std;

// Needs to be called before Main function
int squared(int num) {
    return pow(num, 2);
}

// Main function
int main() {
    int y;
    cout << "Enter a number to be squared: ";
    cin >> y;
    cout << "The square of " << y << " is: " << squared(y);
    return 0;
}