// Adds the digits of the input integers

#include <iostream>

using namespace std;

int main()
{
    int num, sum = 0, m, n;
    cout << "Enter a number: "; // get user input
    cin >> num; // assign the input to num variable
    n = num; // pass the input value for printing

    while(num > 0)
    {
        m = num % 10;
        sum = sum + m;
        num = num / 10;
    }

    // prints the result
    cout << "The sum of " << n << " is " << sum << endl;

    return 0;
}
